'''
Jacob Collins
11-7-15
Design Patterns for Web Programming
Simple Form
'''
import webapp2 #imports webapp2 from GoogleAppEngineLauncher
from form import Form #imports Form() from form.py

class MainHandler(webapp2.RequestHandler):#overall class that launches/runs script
    def get(self):

        f = Form() #set f equal to Form() class to make it easier to call later

        page_head = f.head #sets self.head within Form() equal to variable page_head for easier calling later
        page_body = f.body #sets self.body within Form() equal to variable page_body for easier calling later
        page_close = f.close #sets self.close within Form() equal to variable page_close for easier calling later
        page_export = f.export #sets self.export within Form() equal to variable page_export for easier calling later

        if self.request.GET: #establish if/else statement to determine actions
            name = self.request.GET['name'] #set name equal to input for 'name' in form.py
            address = self.request.GET['address'] #set address equal to input for 'address' in form.py
            number = self.request.GET['number'] #set number equal to input for 'number' in form.py
            phone = self.request.GET['phone'] #set phone equal to input for 'phone' in form.py
            state = self.request.GET['state'] #set state equal to input for 'state' in form.py

            #The following line calls all the previously set variables and their counterparts in form.py to print to the
            #web page for viewing. This calls the HTML created within page_head, page_body, and page_close, exporting the
            #inputs for the viewer to see
            self.response.write(page_head + page_body + page_export + "<p>Name: " + name + "<p>Address: " + address +
                                "<p> Phone Number: " + number + "<p>Contact Type: " + phone + "<p>State: " + state + "</p>" + page_close)
        else:
            self.response.write(page_head + page_body + page_close)
            #This is printed to the web page when the user has not submitted any input. In this instance, we simply call
            #the HTML found within page_head, page_body, and page_close since no information is avaiable to call the
            #page_export variable

#do not change
app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=True)
