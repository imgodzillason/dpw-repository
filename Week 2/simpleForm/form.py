'''
Jacob Collins
11-7-15
Design Patterns for Web Programming
Simple Form
'''
class Form(object): #initialize Form class to call in main.py
    def __init__(self): #self-declaration

        #make self.head accessible outside of class
        self.head = '''
<!DOCTYPE HTML>
<html>
    <link href = "css/styles.css" rel = "stylesheet" type = "text/css">
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,700' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <div class = "container">
            <header>
                <h1>Apply for your <span style = "color:orange;"> Full Sail</span> loan today!</h1>
            </header>
                '''
        #make self.body accessible outside of class
        self.body = '''
            <p>To see if you qualify, submit the following information.</p>
            <form method = GET>
            <p><label>Full Name: </label><br/><input type = "text" name = "name" placeholder = "Joe Cool" /></p>

            <p><label>Address: </label><br/><input type = "text" name = "address" placeholder = "123 Sesame St." /></p>

            <p><label>Number: </label><br/><input type = "text" name = "number" placeholder = "123-456-7890" /></p>


            <p><span>Preferred method of contact: </span><br/>
                <input type = "radio" name = "phone" value = "Cell Phone" id = "cell phone" /><label class = "radio"
                for "cell phone">Cell Phone</label>

                <input type = "radio" name = "phone" value = "Email" id = "email" /><label class = "radio"
                for "email">Email</label>

                <input type = "radio" name = "phone" value = "Snail Mail" id = "snail mail" /><label class = "radio"
                for "snail mail">Snail Mail</label>

                <input type = "radio" name = "phone" value = "Do Not Contact" id = "do not contact" /><label class =
                "radio" for "do not contact">Do Not Contact</label>
            </p>

            <p><label>Please select a state:</label><br/>
                <select name = "state">
                    <option value = "Alabama">Alabama</option>
                    <option value = "Alaska">Alaska</option>
                    <option value = "Arizona">Arizona</option>
                    <option value = "Arkansas">Arkansas</option>
                    <option value = "California">California</option>
                    <option value = "Colorado">Colorado</option>
                    <option value = "Connecticut">Connecticut</option>
                    <option value = "Delaware">Delaware</option>
                    <option value = "Florida">Florida</option>
                    <option value = "Georgia">Georgia</option>
                    <option value = "Hawaii">Hawaii</option>
                    <option value = "Idaho">Idaho</option>
                    <option value = "Illinois">Illinois</option>
                    <option value = "Indiana">Indiana</option>
                    <option value = "Iowa">Iowa</option>
                    <option value = "Kansas">Kansas</option>
                    <option value = "Kentucky">Kentucky</option>
                    <option value = "Louisiana">Louisiana</option>
                    <option value = "Maine">Maine</option>
                    <option value = "Maryland">Maryland</option>
                    <option value = "Massachusetts">Massachusetts</option>
                    <option value = "Michigan">Michigan</option>
                    <option value = "Minnesota">Minnesota</option>
                    <option value = "Mississippi">Mississippi</option>
                    <option value = "Missouri">Missouri</option>
                    <option value = "Montana">Montana</option>
                    <option value = "Nebraska">Nebraska</option>
                    <option value = "Nevada">Nevada</option>
                    <option value = "New Hampshire">New Hampshire</option>
                    <option value = "New Jersey">New Jersey</option>
                    <option value = "New Mexico">New Mexico</option>
                    <option value = "New York">New York</option>
                    <option value = "North Carolina">North Carolina</option>
                    <option value = "North Dakota">North Dakota</option>
                    <option value = "Ohio">Ohio</option>
                    <option value = "Oklahoma">Oklahoma</option>
                    <option value = "Oregon">Oregon</option>
                    <option value = "Pennsylvania">Pennsylvania</option>
                    <option value = "Rhode Island">Rhode Island</option>
                    <option value = "South Carolina">South Carolina</option>
                    <option value = "South Dakota">South Dakota</option>
                    <option value = "Tennessee">Tennessee</option>
                    <option value = "Texas">Texas</option>
                    <option value = "Utah">Utah</option>
                    <option value = "Vermont">Vermont</option>
                    <option value = "Virginia">Virginia</option>
                    <option value = "Washington">Washington</option>
                    <option value = "West Virginia">West Virginia</option>
                    <option value = "Wisconsin">Wisconsin</option>
                    <option value = "Wyoming">Wyoming</option>
                </select></p>

                <input type = "submit" value = "Submit" />
            </form>
                '''

        #make self.export accessible outside of class
        self.export = '''
            <p id = "entered">Please review. You entered: </p>
            <div class = "review">
                '''

        #make self.close accessible outside of class
        self.close = '''
        </div>
        <footer>
                <p>Created by Jacob Collins | Full Sail University 2015</p>
            </footer>

        </div>
    </body>
</html>
                '''