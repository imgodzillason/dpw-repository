'''
Jacob Collins
11-12-15
Design Patterns for Web Programming - Online
Resuable Library
'''

# gathers and stores data from the user inputs


class SalesData(object):
    def __init__(self):
        # all form values input are set to private and given a value for each section of the form
        self.__name = ''
        self.__sales = 0
        self.__leads = 0
        self.__losses = 0
        self.__discounts = 0
        self.__money_won = 0

    # getter created for name so other functions can access the information that will be put into the form


    @property
    def name(self):
        return self.__name

    # setter created for name so other functions can change the values as needed

    @name.setter
    def name(self, p_name):
        self.__name = p_name

    # getter created for sales so other functions can access the information that will be put into the form

    @property
    def sales(self):
        return self.__sales

    # setter created for sales so other functions can change the values as needed

    @sales.setter
    def sales(self, p_sales):
        self.__sales = p_sales

    # getter created for leads so other functions can access the information that will be put into the form

    @property
    def leads(self):
        return self.__leads

    # setter created for leads so other functions can change the values as needed

    @leads.setter
    def leads(self, p_leads):
        self.__leads = p_leads

    # getter created for losses so other functions can access the information that will be put into the form

    @property
    def losses(self):
        return self.__losses

    # setter created for losses so other functions can change the values as needed

    @losses.setter
    def losses(self, p_losses):
        self.__losses = p_losses

    # getter created for discounts so other functions can access the information that will be put into the form

    @property
    def discounts(self):
        return self.__discounts

    # setter created for discounts so other functions can change the values as needed

    @discounts.setter
    def discounts(self, p_discounts):
        self.__discounts = p_discounts

    # getter created for money_won so other functions can access the information that will be put into the form

    @property
    def money_won(self):
        return self.__money_won

    # setter created for money_won so other functions can change the values as needed

    @money_won.setter
    def money_won(self, p_money_won):
        self.__money_won = p_money_won

# deals with calculations from user inputs obtained from SalesData()


class TeamData(object):
    def __init__(self):
        self.__team_list = [] # create an array to store information from SalesData

    def add_member(self, p): # prints out employee name for list to print
        self.__team_list.append(p)
        print (p.name)

    def member_list(self): # all employee data is output here and placed in a list for easier viewing
        output = ''
        for member in self.__team_list:
            output += '<div class = "fullOutput"> ' + '<h2 class = "memberName">' + member.name + '</h2>' + '<p>' + str(member.sales) + ' accounts won.' + '</p>' + '<p>' + str(member.leads) + ' leads.' + '</p>' + '<p>$' + str(member.losses) + ' in losses.' + '</p>' + '<p>$' + str(member.discounts) + ' in discounts.' + '</p>' + '<p>$' + str(member.money_won) + ' total money won.' + '</p>' + '</div>'
        return output

    def calc_sales(self): # add all sales from inputs and returns total
        sales = []
        for member in self.__team_list:
            sales.append(member.sales)
        top_sales = sum(sales)
        return '<p class = "results">' + 'Your team won ' + str(top_sales) + ' sales opportunities this month!' + '</p>'

    def calc_losses(self): # add all losses from inputs and returns average loss after dividing by number of losses input
        losses = []
        for member in self.__team_list:
            losses.append(member.losses)
        avg_loss = (sum(losses)) / len(losses)
        return '<p class = "results">' + 'Your team averaged ' + str(avg_loss) + ' lost accounts this month!' + '</p>'

    def calc_money_won(self): # sorts all input money_won from lowest to highest and determines the amount that the highest employee made compared to the lowest employee
        money_won = []
        for member in self.__team_list:
            money_won.append(member.money_won)
        money_won.sort()
        top_money_won = len(money_won) - 1
        money_won_difference = money_won[top_money_won] - money_won[0]
        return '<p class = "results">' + 'Your top salesperson sold $' + str(money_won_difference) + ' dollars more than your lowest salesperson!' + '</p>'

    def calc_discounts(self): # adds total discounts from inputs and money_won (separately determined) before comparing the two and seeing which is greater. Based on this, it will return a statement
        discounts = []
        for member in self.__team_list:
            discounts.append(member.discounts)
        money_won = []
        for member in self.__team_list:
            money_won.append(member.money_won)
        total_discounts = sum(discounts)
        total_money_won = sum(money_won)

        if total_discounts > total_money_won:
            return '<p class="final">' + 'Your team gave away more discounts than revenue. Upper management will be most displeased!' + '</p>'
        elif total_discounts == total_money_won:
            return '<p class="final">' + 'Your team gave away as many discounts as they made in revenue. Your team essentially did nothing the whole month.' + '</p>'
        else:
            return '<p class="final">' + 'Your team earned more money than they gave away in discounts! Despite what they said about you, it looks like you are not a huge failure after all! ' + '</p>'