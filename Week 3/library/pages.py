class FormPage(object): # form page called if user needs to input values
    def __init__(self):
        self.__title = "Sales Statistical Calculator"
        self.css = "css/style.css"
        # head portion of web page set up
        self.__head = '''

<!DOCTYPE HTML>
<html>
    <head>
        <title>Sales Statistical Calculator</title>
        <link href = "css/styles.css" rel = "stylesheet" type = "text/css">
        <link href='https://fonts.googleapis.com/css?family=Rubik:400,700' rel='stylesheet' type='text/css'>
    </head>
<body>
    <div id = "container">
                <header>
                    <h1 id = "title">Sales Statistical Calculator</h1>
                </header>
                <div id = "formStuff">
                    <p>Enter the information for your team. This will provide you with valuable information!</p>
                </div>
'''
        # body portion of web page set up with fields for user input
        self.body = '''
            <div class = "fullForm">
                <form method= GET>
                    <fieldset>
                    <legend>Employee #1</legend>
                        <p><label>Employee Name: <input type = "text" name = "s1_name" required</label></p>

                        <p><label>Accounts Won: <input type = "number" name = "s1_sales" required</label></p>

                        <p><label>Money Lost (in dollars): <input type = "number" name = "s1_losses" required</label></p>

                        <p><label>Number of Leads: <input type = "number" name = "s1_leads" required</label></p>

                        <p><label>Discounts Given (in dollars): <input type = "number" name = "s1_discounts" required</label></p>

                        <p><label>Revenue Won (in dollars): <input type = "number" name = "s1_money_won" required</label></p>
                    </fieldset>

                    <fieldset>
                    <legend>Employee #2</legend>
                        <p><label>Employee Name: <input type = "text" name = "s2_name" required</label></p>

                        <p><label>Accounts Won: <input type = "number" name = "s2_sales" required</label></p>

                        <p><label>Money Lost (in dollars): <input type = "number" name = "s2_losses" required</label></p>

                        <p><label>Number of Leads: <input type = "number" name = "s2_leads" required</label></p>

                        <p><label>Discounts Given (in dollars): <input type = "number" name = "s2_discounts" required</label></p>

                        <p><label>Revenue Won (in dollars): <input type = "number" name = "s2_money_won" required</label></p>
                    </fieldset>

                    <fieldset>
                    <legend>Employee #3</legend>
                        <p><label>Employee Name: <input type = "text" name = "s3_name" required</label></p>

                        <p><label>Accounts Won: <input type = "number" name = "s3_sales" required</label></p>

                        <p><label>Money Lost (in dollars): <input type = "number" name = "s3_losses" required</label></p>

                        <p><label>Number of Leads: <input type = "number" name = "s3_leads" required</label></p>

                        <p><label>Discounts Given (in dollars): <input type = "number" name = "s3_discounts" required</label></p>

                        <p><label>Revenue Won (in dollars): <input type = "number" name = "s3_money_won" required</label></p>
                    </fieldset>

                    <fieldset>
                    <legend>Employee #4</legend>
                        <p><label>Employee Name: <input type = "text" name = "s4_name" required</label></p>

                        <p><label>Accounts Won: <input type = "number" name = "s4_sales" required</label></p>

                        <p><label>Money Lost (in dollars): <input type = "number" name = "s4_losses" required</label></p>

                        <p><label>Number of Leads: <input type = "number" name = "s4_leads" required</label></p>

                        <p><label>Discounts Given (in dollars): <input type = "number" name = "s4_discounts" required</label></p>

                        <p><label>Revenue Won (in dollars): <input type = "number" name = "s4_money_won" required</label></p>
                    </fieldset>

                    <fieldset>
                    <legend>Employee #5</legend>
                        <p><label>Employee Name: <input type = "text" name = "s5_name" required</label></p>

                        <p><label>Accounts Won: <input type = "number" name = "s5_sales" required</label></p>

                        <p><label>Money Lost (in dollars): <input type = "number" name = "s5_losses" required</label></p>

                        <p><label>Number of Leads: <input type = "number" name = "s5_leads" required</label></p>

                        <p><label>Discounts Given (in dollars): <input type = "number" name = "s5_discounts" required</label></p>

                        <p><label>Revenue Won (in dollars): <input type = "number" name = "s5_money_won" required</label></p>
                    </fieldset>

                    <input type = "submit" value = "Submit">

                </form>
            </div>
            '''
        # closer set up to complete web page
        self.close = '''
            <footer>
                <hr>
                <p>Jacob Collins | Full Sail University 2015</p>
            </footer>
        </div>
    </body>
</html>
'''

    def print_out(self): # prints out entire page
        print_all = self.__head + self.body + self.close
        return print_all

# this prints out if user has input values into the form


class ResultsPage(object):
    def __init__(self):
        self.__title = "Sales Statistical Calculator"
        self.css = "css/style.css"
        # head portion of web page set up
        self.__head = '''

<!DOCTYPE HTML>
<html>
    <head>
        <title>Sales Statistical Calculator</title>
        <link href = "css/styles.css" rel = "stylesheet" type = "text/css">
        <link href='https://fonts.googleapis.com/css?family=Rubik:400,700' rel='stylesheet' type='text/css'>
    </head>
<body>
    <div id = "container2">
        <header>
            <h1>Sales Statistical Calculator</h1>
        </header>
'''
        # body portion of web page set up with fields for user input
        self.body = ""
        # closer set up to complete web page
        self.close = '''
            <footer>
                <hr>
                <p>Jacob Collins | Full Sail University 2015</p>
            </footer>
        </div>
    </body>
</html>
'''

    def print_out(self): # prints out entire page with values
        print_all = self.__head + self.body + self.close
        return print_all
