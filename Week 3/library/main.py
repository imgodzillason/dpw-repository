'''
Jacob Collins
11-12-15
Design Patterns for Web Programming - Online
Reusable Library
'''


import webapp2

#import the classes from library and pages

from library import SalesData, TeamData
from pages import ResultsPage, FormPage


class MainHandler(webapp2.RequestHandler):
    def get(self):
        # create instance of TeamData class
        t = TeamData()

        # set up for data input IF the user inputs something
        if self.request.GET:
            s1 = SalesData()
            # set up user inputs to equal value of variable
            s1.name = self.request.GET['s1_name']
            # make sure inputs are changed to integers/numbers for use in calculations in library.py
            s1.sales = int(self.request.GET['s1_sales'])
            s1.leads = int(self.request.GET['s1_leads'])
            s1.losses = int(self.request.GET['s1_losses'])
            s1.discounts = int(self.request.GET['s1_discounts'])
            s1.money_won = int(self.request.GET['s1_money_won'])
            t.add_member(s1) # adds employee information to TeamData array

            s2 = SalesData()
            # set up user inputs to equal value of variable
            s2.name = self.request.GET['s2_name']
            # make sure inputs are changed to integers/numbers for use in calculations in library.py
            s2.sales = int(self.request.GET['s2_sales'])
            s2.leads = int(self.request.GET['s2_leads'])
            s2.losses = int(self.request.GET['s2_losses'])
            s2.discounts = int(self.request.GET['s2_discounts'])
            s2.money_won = int(self.request.GET['s2_money_won'])
            t.add_member(s2) # adds employee information to TeamData array

            s3 = SalesData()
            # set up user inputs to equal value of variable
            s3.name = self.request.GET['s3_name']
            # make sure inputs are changed to integers/numbers for use in calculations in library.py
            s3.sales = int(self.request.GET['s3_sales'])
            s3.leads = int(self.request.GET['s3_leads'])
            s3.losses = int(self.request.GET['s3_losses'])
            s3.discounts = int(self.request.GET['s3_discounts'])
            s3.money_won = int(self.request.GET['s3_money_won'])
            t.add_member(s3) # adds employee information to TeamData array

            s4 = SalesData()
            # set up user inputs to equal value of variable
            s4.name = self.request.GET['s4_name']
            # make sure inputs are changed to integers/numbers for use in calculations in library.py
            s4.sales = int(self.request.GET['s4_sales'])
            s4.leads = int(self.request.GET['s4_leads'])
            s4.losses = int(self.request.GET['s4_losses'])
            s4.discounts = int(self.request.GET['s4_discounts'])
            s4.money_won = int(self.request.GET['s4_money_won'])
            t.add_member(s4) # adds employee information to TeamData array

            s5 = SalesData()
            # set up user inputs to equal value of variable
            s5.name = self.request.GET['s5_name']
            # make sure inputs are changed to integers/numbers for use in calculations in library.py
            s5.sales = int(self.request.GET['s5_sales'])
            s5.leads = int(self.request.GET['s5_leads'])
            s5.losses = int(self.request.GET['s5_losses'])
            s5.discounts = int(self.request.GET['s5_discounts'])
            s5.money_won = int(self.request.GET['s5_money_won'])
            t.add_member(s5)# adds employee information to TeamData array

            #if user inputs are obtained, then we'll load the information
            r = ResultsPage()
            #add in all information obtained from TeamData to add to the results print out
            r.body = t.member_list() + t.calc_sales() + t.calc_losses() + t.calc_money_won() +t.calc_discounts()
            self.response.write(r.print_out())
            #if no information has been gathered, this will load the forms for user input
        else:
            r = FormPage()
            self.response.write(r.print_out())


app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=True)